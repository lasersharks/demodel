deModel Release 0.2.2
===================

Introduction
------------

The Python package deModel provides a fixed point data type for Python,
allowing the develop of algorithms using fixed point arithmetic. The
basic data type is represented by the class DeFixedInt, which stores
data as an integer and keeps information about the decimal point. When
performing basic arithmetic operations with this data type, the decimal
point is adjusted based on some fundamental rules of fixed point
arithmetic. For further details about those rules we recommend a paper
by Randy Yates titled "Fixed Point Arithmetic: An Introduction",
available from the Digital Signal Labs page. 

http://www.digitalsignallabs.com/fp.pdf


Installation
------------

The installation of the package is fairly simple. When using the
platform-independent source you can use the git command to fetch the source.

~> git clone git@bitbucket.org:lasersharks/demodel.git

Then change into the newly created directory and run the setup.py script:

~> cd demodel/
~/demodel> python setup.py install

Documentation
-------------

See the code documentation, available through pydoc. On Windows look at the 
source or something.

On Linux enter on the command line:

> pydoc -g &

Hit the "open browser" button in the opening window, which in turn will
open the browser with the Python module documentation. Search for the
deModel package.

Author
------

Dillon Engineering, Inc. <info@dilloneng.com>
Allen Blaylock <allenb@epiloglaser.com>

